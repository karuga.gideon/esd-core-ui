import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Link } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import { isLoggedIn, getUserDetails } from "../../../util/AuthService";
import { API_BASE_URL } from "../../../constants";
import axios from "axios";

class InvoiceDetails extends React.Component {
  constructor(props) {
    super(props);
    document.title = "User Details";

    var invoiceID = this.props.match.params.invoiceID;
    console.log("invoiceID >>> " + invoiceID);

    this.state = {
      records: [],
      invoice_details: {},
      isLoading: false,
      pageOfItems: [],
      invoiceID: invoiceID,      
      backgroundColor: "black",
      activeColor: "warning"
    };
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  componentDidMount() {

    this.checkAuth(); 
    var loggedIn = getUserDetails();       
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    console.log(securityToken);
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    const { invoiceID } = this.state;
    console.log("Getting Invoice Details >>> " + invoiceID);

    axios({
      url: `${API_BASE_URL}/invoices/id/` + invoiceID,
      method: "GET",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        var json_data = JSON.parse(JSON.stringify(response.data));
        console.log(json_data);

        if (!response.data.error) {
          this.setState({ 
              invoice_details: json_data, 
              userUpdateName: json_data.name, 
              userUpdatePhone: json_data.phone, 
              userUpdateEmail: json_data.email, 
              userUpdateUsername: json_data.username  });
          console.log(json_data);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  }

    
  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }

  
  onSubmit = e => {

    this.setState({ isLoading: true });   
    const { invoice_details, invoiceID } = this.state;

    var userPayLoad = {
        name: this.state.userUpdateName,
        phone: this.state.userUpdatePhone,
        email: this.state.userUpdateEmail,
        username: this.state.userUpdateUsername
    };

    var loggedIn = getUserDetails();
    console.log("Getting user details....?");
    console.log(loggedIn);
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }

    console.log(securityToken);
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;

    console.log("Updating User ID Details >>> " + invoiceID);
    console.log(invoice_details);

    axios({
      url: `${API_BASE_URL}/users/` + invoiceID,
      method: "PUT",
      withCredentials: false,
      headers: {
        "Content-Type": "application/json"
      },
      data: userPayLoad
    })
      .then(response => {
        this.setState({ isLoading: false });
        console.log(response.data);
        if (!response.data.error) {
          alert("User Updated Successfully.");
          this.props.history.push("/user-details/" + invoiceID);
        } else {
          alert(response.data.details);
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
      });
  };

  render() {

    const { invoice_details, invoiceID } = this.state;

    return (
      
      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">        
            
            { loadHeader() }

        <div className="app-body">
          
          { loadSidebarMenu() }

          <main className="main"> 
      
        {/* <Sidebar
          {...this.props}
          routes={dashboardRoutes}
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
        /> */}

        <div
          className="main-panel"
          ref="mainPanel"
          style={{ overflowX: "hidden", msOverflowY: "hidden" }}
        >
          
          
          {/* <!-- Breadcrumb--> */}
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item">
              <Link to="dashboard">Admin</Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="invoices">Invoices</Link>
            </li>
            <li className="breadcrumb-item active">Invoice Details</li>
            {/* <!-- Breadcrumb Menu--> */}
            <li className="breadcrumb-menu d-md-down-none">
              <div className="btn-group" role="group" aria-label="Button group">
                <Link className="btn" to="dashboard">
                  <i className="icon-speech"></i>
                </Link>
                <Link className="btn" to="/dashboard">
                  <i className="icon-graph"></i>  Dashboard</Link>
                <Link className="btn" to="/users">
                  <i className="icon-settings"></i>  Settings</Link>
              </div>
            </li>
          </ol>
          <div className="container-fluid">

            <Row>
              <Col md={8} xs={8}>
                <Card className="card-user">  
                  <CardHeader>
                    <CardTitle>Invoice Details</CardTitle>
                  </CardHeader>                
                  <CardBody>
                    <p className="description" style={{ marginTop: "20px" }}>
                      
                      Invoice ID : {invoiceID} <br />
                      Invoice Number : {invoice_details.invoice_number} <br />
                      VAT : {invoice_details.vat} <br />
                      Grand Total : {invoice_details.grand_total} <br />
                      Signature : {invoice_details.signature} <br />
                      <br />
                      <h6>Date Created </h6>
                      {invoice_details.date_created}

                      <br/>
                      <br/>
                      <hr/>

                    </p>
                  </CardBody>
                  <CardFooter />
                </Card>
              </Col>
              <Col md={4} xs={4}>
              <Card className="card-user">
                  <CardHeader>
                    <CardTitle>Invoices</CardTitle>
                  </CardHeader>
                  <CardBody>
                    
                      <div style={{ textAlign: "left"}}>
                        <Link
                          to="/invoices"
                          className="btn btn-primary btn-xl text-uppercase js-scroll-trigger"
                          style={{ backgroundColor: "#fed136", width: "70%" }}
                        >
                          <Menu.Item name="Back" />
                        </Link>
                      </div>  

                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>

          <FixedPlugin
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
            handleActiveClick={this.handleActiveClick}
            handleBgClick={this.handleBgClick}
          />
        </div>
        </main>
        </div>
      </div>
    );
  }
}

export default InvoiceDetails;
