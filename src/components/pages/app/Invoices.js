import React from "react";
import { Card, CardBody, CardHeader, CardTitle, Row, Col } from "reactstrap";
import FixedPlugin from "../../admin/FixedPlugin/FixedPlugin.jsx";
import { Link } from "react-router-dom";
import { Table, Form } from "semantic-ui-react";
import { loadHeader, loadSidebarMenu } from "../../shared/Includes.js";

import axios from "axios";
import { API_BASE_URL } from "../../../constants";
import Loader from "../../shared/Loader";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { getUserDetails, isLoggedIn } from "../../../util/AuthService";
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';


window.React = React;


export class CommentList extends React.Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
  };

  render() {    
    let commentNodes = this.props.data.map(function(invoice, index) {
      return <div key={index}>
          {invoice.id} &nbsp; | &nbsp; 
          {invoice.date_created} &nbsp; | &nbsp;  
          {invoice.invoice_number}
        </div>;
    });

    return (
      <div id="project-comments" className="commentList">
        <ul>{commentNodes}</ul>
      </div>
    );
  }
}

class Invoices extends React.Component {
  constructor(props) {
    super(props);
    document.title = "Invoices";
    this.state = {
      backgroundColor: "black",
      activeColor: "warning",
      records: [],
      currentRecords: [],
      data: [],
      offset: 0,
      recordsSearch: [],
      recordsCount: 0, 
      graphArray: [],
      isLoading: false,
      pageOfItems: [], 
      currentPage: 1,
      recordsPerPage: 5, 
      limit: 5, 
      startDate: new Date(), 
      endDate: new Date(), 
      reportTypes: ["Export", "Excel", "CSV"], 
      reportExportType: "Excel" 
    };

    this.handleSearch = this.handleSearch.bind(this);
    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
    this.handleDateFilter = this.handleDateFilter.bind(this);
    this.handleReportExport = this.handleReportExport.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  
  handleSearch(e) {

    const { limit } = this.state;

    var queryParameter = e.target.value;
    console.log("Searching by >>> " + queryParameter);

    if(queryParameter.length === 0){
      queryParameter = "query"
    }

    this.checkAuth();
    var loggedIn = getUserDetails();
    console.log("Getting user details....?");
    console.log(loggedIn);
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    console.log(securityToken);
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
    console.log("Getting invoices....?");

    // invoices/{limit}/{offset}/{search}    

    axios({
      url: API_BASE_URL + "/invoices/5/0/"+queryParameter,
      method: "GET",
      withCredentials: false
    })
      .then(response => {
        // this.props.toggleLoader();
        var json_data = JSON.parse(JSON.stringify(response.data));
        console.log(json_data);

        if (!response.data.error) {
          this.setState({ records: json_data.invoices });
          this.setState({ data: json_data.invoices });
          this.setState({ currentRecords: json_data.invoices });          
          this.setState({ recordsCount: json_data.invoices_count });
          this.setState({ offset: (json_data.offset + limit) });                  
          console.log(json_data);
          this.setState({ recordsSearch: json_data.invoices });
          // pageCount: Math.ceil(data.meta.total_count / data.meta.limit),
          // this.setState({ pageCount: Math.ceil(json_data.invoices_count / limit ) });          
          this.setState({ pageCount: json_data.num_pages });          
        }
      })
      .catch(err => {
        console.log(err);
      });
  }


  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  handleActiveClick = color => {
    this.setState({ activeColor: color });
  };

  handleBgClick = color => {
    this.setState({ backgroundColor: color });
  };

  handleStartDateChange(date) {    
    this.setState({
      startDate: date
    });
  }

  handleEndDateChange(date) {
    this.setState({
      endDate: date
    });
  }

  handleReportExport(event){
    
    var reportType = event.target.value.toLowerCase(); 
    console.log("Downloading report >>> " + reportType);

    var loggedIn = getUserDetails();
    var startDate = Date.parse(this.state.startDate); 
    var endDate = Date.parse(this.state.endDate); 
    console.log("Users date filter report search...");
    console.log("startDate >>>  " + startDate);
    console.log("endDate   >>>> " + endDate);
    console.log(loggedIn);
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    console.log(securityToken);
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
    console.log("Getting invoices....?");

    axios({
      url: API_BASE_URL + "/export/invoices/" + reportType + "/" + startDate + "/" + endDate,
      method: "GET",
      withCredentials: false
    })
      .then(response => {
        // this.props.toggleLoader();
        // var json_data = JSON.parse(JSON.stringify(response.data));
        // console.log(json_data);

        if(reportType === "excel"){
          console.log("Downloading excel report...");
          this.downloadURL("http://localhost:9010/downloads/Invoices_Excel.xlsx");
        }

        if(reportType === "csv"){
          console.log("Downloading CSV Report...");
          // var url = "http://localhost:9010/downloads/Invoices_CSV.csv";
          // this.downloadURI(url);
          document.getElementById('download_csv').click();
          // document.getElementById('my_iframe').src = url;
        }
        
        
      })
      .catch(err => {
        console.log(err);
      });      
  }

  downloadURL(url) {
      var hiddenIFrameID = 'hiddenDownloader',
          iframe = document.getElementById(hiddenIFrameID);
      if (iframe === null) {
          iframe = document.createElement('iframe');
          iframe.id = hiddenIFrameID;
          iframe.style.display = 'none';
          document.body.appendChild(iframe);
      }
      iframe.src = url;
  };

  
  downloadURI(uri, name) 
  {
      var link = document.createElement("a");
      link.download = name;
      link.href = uri;
      link.click();
  }

  handleDateFilter(){

    var loggedIn = getUserDetails();
    var startDate = Date.parse(this.state.startDate); 
    var endDate = Date.parse(this.state.endDate); 
    console.log("Users date filter report search...");
    console.log("startDate >>>  " + startDate);
    console.log("endDate   >>>> " + endDate);
    console.log(loggedIn);
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    console.log(securityToken);
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
    console.log("Getting invoices....?");

    axios({
      url: API_BASE_URL + "/invoices/" + startDate + "/" + endDate,
      method: "GET",
      withCredentials: false
    })
      .then(response => {
        // this.props.toggleLoader();
        var json_data = JSON.parse(JSON.stringify(response.data));
        console.log(json_data);

        if (!response.data.error) {
          this.setState({ records: json_data.invoices });
          this.setState({ recordsCount: json_data.invoices_count });
          console.log(json_data);
          this.setState({ recordsSearch: json_data.invoices });
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentDidMount() {
  
    const { limit, offset } = this.state;

    this.checkAuth();
    var loggedIn = getUserDetails();
    console.log("Getting user details....?");
    console.log(loggedIn);
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    console.log(securityToken);
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
    console.log("Getting invoices....?");

    // invoices/{limit}/{offset}/{search}    

    axios({
      url: API_BASE_URL + "/invoices/" + limit + "/" + offset + "/query",
      method: "GET",
      withCredentials: false
    })
      .then(response => {
        // this.props.toggleLoader();
        var json_data = JSON.parse(JSON.stringify(response.data));
        console.log(json_data);

        if (!response.data.error) {
          this.setState({ records: json_data.invoices });
          this.setState({ data: json_data.invoices });
          this.setState({ currentRecords: json_data.invoices });          
          this.setState({ recordsCount: json_data.invoices_count });
          this.setState({ offset: (json_data.offset + limit) });                  
          console.log(json_data);
          this.setState({ recordsSearch: json_data.invoices });
          // pageCount: Math.ceil(data.meta.total_count / data.meta.limit),
          // this.setState({ pageCount: Math.ceil(json_data.invoices_count / limit ) });          
          this.setState({ pageCount: json_data.num_pages });          
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  handlePageClick = data => {    
    this.loadInvoicesFromServer(data);
  };


  loadInvoicesFromServer(data) {
    
    var selectedPage =  data.selected + 1;
    console.log("selected page  >>>> " + selectedPage);

    const { limit, offset } = this.state;

    this.checkAuth();
    var loggedIn = getUserDetails();
    console.log("Getting user details....?");
    console.log(loggedIn);
    var securityToken = "";

    if (loggedIn != null) {
      securityToken = loggedIn.token;
    }
    
    console.log(securityToken);
    axios.defaults.headers.common["Authorization"] = "Bearer " + securityToken;
    console.log("Getting invoices....?");

    // invoices/{limit}/{offset}/{search}

    console.log("Limit: " + limit +", offset: " + offset +", selectedPage: " + selectedPage );

    var calculatedOffset = (selectedPage * limit) - limit;
    console.log("calculatedOffset  >>> " + calculatedOffset);

    axios({
      url: API_BASE_URL + "/invoices/" + limit + "/" + calculatedOffset + "/query",
      method: "GET",
      withCredentials: false
    })
      .then(response => {
        // this.props.toggleLoader();
        var json_data = JSON.parse(JSON.stringify(response.data));
        console.log(json_data);

        if (!response.data.error) {
          this.setState({ records: json_data.invoices });
          this.setState({ data: json_data.invoices });
          this.setState({ currentRecords: json_data.invoices });          
          this.setState({ recordsCount: json_data.invoices_count });      
          console.log(json_data);
          this.setState({ recordsSearch: json_data.invoices });     
          this.setState({ pageCount: json_data.num_pages });    
          this.setState({ offset: calculatedOffset });          
        }
      })
      .catch(err => {
        console.log(err);
      });
  }


  componentWillMount() {
    this.checkAuth();
  }

  checkAuth() {
    var loggedIn = isLoggedIn();
    if (!loggedIn) {
      this.props.history.push("/");      
    } else {
      var userDetails = getUserDetails(); 
      this.setState({ 
        name: userDetails.user.name, 
        phone: userDetails.user.phone, 
        email: userDetails.user.email  });  
    }
  }


  static propTypes = {
    data: PropTypes.array.isRequired,
  };

  render() {
    
    const { currentRecords, records, recordsPerPage, recordsCount } = this.state;

    
    // Logic for displaying page numbers
    const recordsPageNumbers = [];
    for (let i = 1; i <= Math.ceil(records.length / recordsPerPage); i++) {
      recordsPageNumbers.push(i);
    }

    let searchBox = (    

      <div style={{ float:"right", clear:"both" }}>      

        <input style={{ marginRight: "10px", borderRadius: "5px", fontSize: "12pt", float: "right" }} 
          type="text" className="input" placeholder="Search..." onChange={this.handleSearch} />

      </div>);

    let createUserButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <div style={{ paddingBottom: "30px" }}>       
     
      <div style = {{ float: "right", marginRight: "0px", zIndex: "6" }}>

      <Row>

        <Col md={4} xs={4}>
          <DatePicker
              selected={this.state.startDate}
              onChange={this.handleStartDateChange}
              dateFormat="dd/MM/yyyy" 
              // minDate= { subDays(new Date(), 5) }
              // maxDate= { new Date()}
              style={{ zIndex: "5"}}
          />
        </Col>

        <Col md={4} xs={4} style={{ marginLeft: "-10px" }}>
          <DatePicker
              selected={this.state.endDate}
              onChange={this.handleEndDateChange}
              dateFormat="dd/MM/yyyy" 
              // minDate= { subDays(new Date(), 5) }
              // maxDate= { new Date()}
              style={{ zIndex: "5"}}
          />
        </Col>

        <Col md={1} xs={1} style={{ marginLeft: "-12px" }}>
            <Form.Field
              primary
              className="field btn btn-primary btn-sm text-uppercase"
              type="submit"
              color="blue"
              style={{ backgroundColor: "#20a8d8" }}
              onClick={this.handleDateFilter}
            >
              Filter
            </Form.Field>
        </Col>

        <Col md={1} xs={1} style={{ marginLeft: "15px" }}>
        <select style={{ height: "28px"}} >
              { this.state.reportTypes.map(
                        (reportType) => 
                        <option key={reportType} value={reportType} onClick={this.handleReportExport}>{ reportType }</option>) }                                
        </select>           
        </Col>       

      </Row>
        
      </div>
      
    </div>
    );

    let viewButton = this.state.isLoading ? (
      <Loader />
    ) : (
      <Form.Field
        primary
        className="field btn btn-primary btn-xl text-uppercase"
        type="submit"
        color="green"
        style={{ backgroundColor: "#6bd098" }}
        onClick={this.onSubmit}
      >
        View
      </Form.Field>
    );

    return (
      <div className="">

      <div className="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">        
            
            { loadHeader() }

        <div className="app-body">
          
          { loadSidebarMenu("/invoices-001") }

          {/* <Sidebar
            {...this.props}
            routes={dashboardRoutes}
            bgColor={this.state.backgroundColor}
            activeColor={this.state.activeColor}
          /> */}

          <main className="main">

        
        <div className="main-panel" ref="mainPanel">
          
          {/* <!-- Breadcrumb--> */}
          <ol className="breadcrumb">
            <li className="breadcrumb-item">Home</li>
            <li className="breadcrumb-item">
              <Link to="dashboard">Admin</Link>
            </li>
            <li className="breadcrumb-item active">Invoices</li>
            {/* <!-- Breadcrumb Menu--> */}
            <li className="breadcrumb-menu d-md-down-none">
              <div className="btn-group" role="group" aria-label="Button group">
                <Link className="btn" to="dashboard">
                  <i className="icon-speech"></i>
                </Link>
                <Link className="btn" to="dashboard">
                  <i className="icon-graph"></i>  Dashboard</Link>
                <Link className="btn" to="invoices">
                  <i className="icon-settings"></i>  Settings</Link>
              </div>
            </li>
          </ol>
          
          <div className="container-fluid">

            <Row>
              <Col xs={12}>
                <Card style={{ height: "100%"}}>
                  <CardHeader>
                    <CardTitle tag="h5">Invoices ({recordsCount}) &nbsp; {searchBox} </CardTitle>                    
                  </CardHeader>
                  <CardBody style={{ overflowX: "hidden", msOverflowY: "auto", height: "100%" }}> 

                  {createUserButton}          

                  <a href="http://localhost:9010/downloads/Invoices_CSV.csv" download id="download_csv" hidden>Download</a>


                    <br/>
                    <Table
                      celled
                      selectable
                      responsive
                      style={{ overflowX: "hidden", msOverflowY: "hidden" }}
                    >
                      <Table.Header>
                        <Table.Row style={{ color: "#51cbce" }}>
                          <Table.HeaderCell>Invoice Number</Table.HeaderCell>
                          <Table.HeaderCell>VAT</Table.HeaderCell>
                          <Table.HeaderCell>Grand Total</Table.HeaderCell>
                          <Table.HeaderCell>Date Created</Table.HeaderCell>                          
                          <Table.HeaderCell>Signature</Table.HeaderCell>        
                          <Table.HeaderCell>Action</Table.HeaderCell>
                        </Table.Row>
                      </Table.Header>

                      <Table.Body>
                        {currentRecords.map(record => (
                          <Table.Row>
                            <Table.Cell>{record.invoice_number}</Table.Cell>
                            <Table.Cell>{record.vat}</Table.Cell>
                            <Table.Cell>{record.grand_total}</Table.Cell>                            
                            <Table.Cell>{record.date_created}</Table.Cell>
                            <Table.Cell>{record.signature.substring(1, 20)}</Table.Cell>                            
                            <Table.Cell>
                              <Link to={`/invoice/${record.id}`}>
                                {viewButton}
                              </Link>                              
                            </Table.Cell>
                          </Table.Row>
                        ))}
                      </Table.Body>
                    </Table>
                  
                    <br/>

                      <div className="container">
                            {/* <CommentList data={this.state.data} /> */}
                            <nav aria-label="Page navigation example"> 
                              <ReactPaginate                                                        
                                previousLabel={'Previous'}
                                nextLabel={'Next'}
                                breakLabel={'...'}
                                breakClassName={'break-me'}
                                pageCount={this.state.pageCount}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={this.handlePageClick}
                                containerClassName={'pagination'}
                                subContainerClassName={'pages pagination page-item'}
                                activeClassName={'active'}
                                pageClassName="page-item"
                                pageLinkClassName="page-link"                              
                                disabledClassName="disabled"
                                nextClassName="page-link"
                                previousClassName="page-link"
                              />
                          </nav>   
                    </div>
                    
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>
        </div>
        <FixedPlugin
          bgColor={this.state.backgroundColor}
          activeColor={this.state.activeColor}
          handleActiveClick={this.handleActiveClick}
          handleBgClick={this.handleBgClick}
        />

        </main>
        </div>
        </div>
      </div>
    );
  }
}

export default Invoices;
